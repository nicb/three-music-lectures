# Three Music Lectures

![CC BY-SA](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

Three lectures on contemporary music composition and its relation with modern technologies.

## Description (Abstract)

Modern technologies, and in particular analog and then digital electronics,
along with the invention of sound reproduction,
have radically transformed the way composers think about composition.

The traditional spaces of composition (pitch and rhythm) are no longer
segmented *per se* and segmentation becomes a compositional choice.

Electronic reproduction reinforces the connotational aspects of sound placing
them at the center of some compositional attitudes.

Finally, instrumental tooling changes radically: composers can now leverage a
considerable palette of new tools, both on the compositional and on the
instrumental side.

These three considerations are at the center of the *Three Music Lectures*.

## Delivered

1. Universität für Musik und darstellende Kunst, Wien, May 2-4 2018 (in english)
2. Università degli Studi di Napoli "L'Orientale", Napoli, 25 Maggio 2022 (abridged version, in italian) [YouTube video](https://youtu.be/T8_RaQFRXTo)
2. Università degli Studi di Napoli "L'Orientale", Napoli, 04 Aprile 2023 (abridged version, in italian)

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img
alt="Creative Commons License" style="border-width:0"
src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This
work is licensed under a <a rel="license"
href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons
Attribution-ShareAlike 4.0 International License</a>.

Please read the [License](./LICENSE.md) for more information.
