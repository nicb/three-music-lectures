1. la gestione di domini continui (che è *il* problema centrale  che
   si apre con le tecnologie elettroacustiche):
   Stockhausen -> Kontakte (unico lavoro "analogico" ma per via della riflessione sull'unità del tempo musicale  di  KS),  Xenakis  (devo  ancora  scegliere),   Grisey   (Modulations), Smalley (Wind Chimes) spectromorfology, Penderecki Threnody, Scelsi (Pfhat?), Razzi Progetto Secondo
     

1. l'assorbimento e la "digestione compositiva" del fenomeno della
      riproducibilità:
      Ives "Central Park in the Dark", Berio Sinfonia III movimento, Snitke Concerto Grosso n.1 primo movimento, musique concrete, Berio THEMA,   Bernardini "Partita" per voce sola

1. le tecnologie  informatiche  per  la  composizione  assistita  (su
      questo aspetto ho un punto di vista abbastanza eccentrico rispetto
      alla norma):
      barriera della complessita`, flussi e processi contro fasce e droni
