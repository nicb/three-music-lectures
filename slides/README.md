# Instructions to read these slides

* go to folder versions/20180502-04-mdw-Wien/
* open Bernardini-MdW-slides-20180503-05.pdf with your favourite pdf reader
* you will find some "speaker" icons here and there in the slides: by clicking
  on them you should be able to listen/view the excerpts
